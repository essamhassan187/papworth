package entities;
// Generated May 7, 2015 12:33:40 PM by Hibernate Tools 4.3.1


import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

/**
 * Email generated by hbm2java
 */
@Entity
@Table(name="email"
    ,catalog="webmail"
)
public class Email  implements java.io.Serializable {


     private int emailid;
     private Date timestamp;
     private Folder folder;
     private User user;
     private String subject;
     private String content;
     private Boolean important;
     private Boolean read;
     private Set<User> recievers = new HashSet<User>(0);
     private Set<Attachment> attachments = new HashSet<Attachment>(0);
     private Set<User> BCC = new HashSet<User>(0);
     private Set<User> CC = new HashSet<User>(0);

    public Email() {
    }

	
    public Email(int emailid, Folder folder, User user) {
        this.emailid = emailid;
        this.folder = folder;
        this.user = user;
    }
    public Email(int emailid, Folder folder, User user, String subject, String content, Boolean important, Boolean read, Set<User> users, Set<Attachment> attachments, Set<User> users_1, Set<User> users_2) {
       this.emailid = emailid;
       this.folder = folder;
       this.user = user;
       this.subject = subject;
       this.content = content;
       this.important = important;
       this.read = read;
       this.recievers = users;
       this.attachments = attachments;
       this.BCC = users_1;
       this.CC = users_2;
    }
   
     @Id 

    
    @Column(name="emailid", unique=true, nullable=false)
    public int getEmailid() {
        return this.emailid;
    }
    
    public void setEmailid(int emailid) {
        this.emailid = emailid;
    }

    @Version@Temporal(TemporalType.TIMESTAMP)
    @Column(name="timestamp", length=19)
    public Date getTimestamp() {
        return this.timestamp;
    }
    
    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="folderid", nullable=false)
    public Folder getFolder() {
        return this.folder;
    }
    
    public void setFolder(Folder folder) {
        this.folder = folder;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="senderid", nullable=false)
    public User getUser() {
        return this.user;
    }
    
    public void setUser(User user) {
        this.user = user;
    }

    
    @Column(name="subject", length=45)
    public String getSubject() {
        return this.subject;
    }
    
    public void setSubject(String subject) {
        this.subject = subject;
    }

    
    @Column(name="content", length=16777215)
    public String getContent() {
        return this.content;
    }
    
    public void setContent(String content) {
        this.content = content;
    }

    
    @Column(name="important")
    public Boolean getImportant() {
        return this.important;
    }
    
    public void setImportant(Boolean important) {
        this.important = important;
    }

    
    @Column(name="read")
    public Boolean getRead() {
        return this.read;
    }
    
    public void setRead(Boolean read) {
        this.read = read;
    }

@ManyToMany(fetch=FetchType.LAZY)
    @JoinTable(name="recievers", catalog="webmail", joinColumns = { 
        @JoinColumn(name="emailid", nullable=false, updatable=false) }, inverseJoinColumns = { 
        @JoinColumn(name="recieverid", nullable=false, updatable=false) })
    public Set<User> getRecievers() {
        return this.recievers;
    }
    
    public void setRecievers(Set<User> recievers) {
        this.recievers = recievers;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="email")
    public Set<Attachment> getAttachments() {
        return this.attachments;
    }
    
    public void setAttachments(Set<Attachment> attachments) {
        this.attachments = attachments;
    }

@ManyToMany(fetch=FetchType.LAZY)
    @JoinTable(name="BCC", catalog="webmail", joinColumns = { 
        @JoinColumn(name="emailid", nullable=false, updatable=false) }, inverseJoinColumns = { 
        @JoinColumn(name="bccid", nullable=false, updatable=false) })
    public Set<User> getBCC() {
        return this.BCC;
    }
    
    public void setBCC(Set<User> BCC) {
        this.BCC = BCC;
    }

@ManyToMany(fetch=FetchType.LAZY)
    @JoinTable(name="CC", catalog="webmail", joinColumns = { 
        @JoinColumn(name="emailid", nullable=false, updatable=false) }, inverseJoinColumns = { 
        @JoinColumn(name="ccid", nullable=false, updatable=false) })
    public Set<User> getCC() {
        return this.CC;
    }
    
    public void setCC(Set<User> CC) {
        this.CC = CC;
    }




}


