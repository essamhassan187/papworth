/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpers;

import entities.Email;
import entities.User;
import hibernatecfg.HibernateUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author essam
 */

public class MailBox {
    Session session = null;

    public MailBox() {
        this.session = HibernateUtil.getSessionFactory().getCurrentSession();
    }
    
    public List<Email> getUserInbox(User currentUser){
        List <Email> inbox=new ArrayList<>();
        inbox.addAll(currentUser.getInbox());
        inbox.addAll(currentUser.getCcd());
        inbox.addAll(currentUser.getBccd());
        return inbox;
    }
    
     public List<Email> getUserSent(User currentUser){
        List <Email> sent=new ArrayList<>();
        sent.addAll(currentUser.getSent());
        return sent;
    }
     
}
