
package helpers;

import entities.User;
import hibernatecfg.HibernateUtil;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author essam
 */
public class UserHelper {
    Session session = null;

    public UserHelper() {
        this.session = HibernateUtil.getSessionFactory().openSession();
    }
    
    public int authenticate(String username,String password){
        List<User> resUsers=null;
           session.beginTransaction();
            User usr = (User) session
             .createQuery("from User as user where user.username = '"+username+"' ").uniqueResult();
            session.getTransaction().commit();
            if(usr.getPassword().equals(password)){
                    return usr.getUserid();
            }
        return -1;
    }
    
}
