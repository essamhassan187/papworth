/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import entities.User;
import hibernatecfg.HibernateUtil;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import org.hibernate.Session;

/**
 *
 * @author essam
 */
@ManagedBean(name = "CurrentUser")
@ApplicationScoped
public class CurrentUser{

    private static User currentUser;
    private static Session session = HibernateUtil.getSessionFactory().openSession();
    private CurrentUser() {
    }
    public static boolean setCurrentUser(int usrid){
        session.beginTransaction();
        currentUser=(User) session.get(User.class, usrid);
        return false;
    }
    public static User getCurrentUser(){
        return currentUser;
    }
    
    
}
