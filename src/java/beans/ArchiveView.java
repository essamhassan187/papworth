/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

 //package org.primefaces.showcase.view.datatable.data;
 
import entities.Email;
import entities.Folder;
import entities.User;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
//import org.primefaces.showcase.domain.Car;
//import org.primefaces.showcase.service.CarService;
 

@ManagedBean(name="ArchiveSelectionView")
@ViewScoped

 public class ArchiveView implements Serializable {
     
    private List<Email> emails;
    private List<Email> selectedEmails;
     @ManagedProperty("#{MailBoxService}")
    private MailBoxService service;
   
    @PostConstruct
    public void init() {
        emails=new ArrayList<>();
        emails.add(new Email(1, new Folder(1), new User(1, "essamhassan", "hamadahelal")));
        emails.get(0).setContent("hello worled");
        emails.get(0).setSubject("Dear Hello jobasjb");
         emails.add(new Email(2, new Folder(1), new User(2, "amroshehata", "entaalbakasy")));
        emails.get(1).setContent("hello manga");
        emails.get(1).setSubject("Dear Hello akl akl akl");
        System.out.println(emails.toString());
    }
     
    public void setService(MailBoxService service) {
        this.service = service;
    }
    
     public List<Email> getEmails() {
        return emails;
    }
 
    public void setEmails(List<Email> emails) {
        this.emails = emails;
    }
 
    public List<Email> getSelectedEmails() {
        return selectedEmails;
    }
 
    public void setSelectedEmails(List<Email> selectedEmails) {
        this.selectedEmails = selectedEmails;
    }
     
    public void onRowSelect(SelectEvent event) {
        FacesMessage msg = new FacesMessage("Email Selected", ((Email) event.getObject()).getSubject());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
 
    public void onRowUnselect(UnselectEvent event) {
        FacesMessage msg = new FacesMessage("Email Unselected", ((Email) event.getObject()).getSubject());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
}